import json
import requests
import re
from flask import (current_app, g, request)



# Perform Solr query based on request arguments (flask global),
# configuration defaults and overrides, 
# and any extra_parameters needed (list of tuples)
def select_request(extra_params = [], config_override={}):
    solr = get_solr() 
    solr = "/".join([solr,'select']) 
    if request.method == 'GET':
        params = get_request_params()

        for key,value in extra_params:
            process_in_params(key,value,params)           

        # process configuration + ad hoc overrides
        if 'SOLR_PARAMS_DEFAULT' in current_app.config:
            for edit_key,p in current_app.config['SOLR_PARAMS_DEFAULT'].items():
                if edit_key in config_override:
                    p = config_override[edit_key]

                for key,value in p.items():
                    process_in_params(key,value,params)           

        # force single value for q
        if isinstance(params["q"],list):
            params["q"] = params["q"].pop()

        r = requests.post(solr,params)  

    if r.status_code != 200:
        return r    
    return r.json() 



def process_in_params(key,value, params):
    if key in params:
        if not isinstance(params[key],list):
            params[key] = [params[key]]

        if isinstance(value,list):
            params[key].extend(value)
        else:
            params[key].append(value)
    else:
        params[key] = value   



# remove all but explicitly allowed user generated params
def get_request_params():
    params = request.args.to_dict(flat=False)
    sanitized = {}
    if 'SOLR_ALLOWED_USER_PARAMS' in current_app.config:
        for key in params:
            if key in current_app.config['SOLR_ALLOWED_USER_PARAMS']:
                sanitized[key] = params[key]
    return sanitized 



def select(params):
    solr = get_solr() 
    solr = "/".join([solr,'select']) 
    r = requests.get(solr,params)  
    return r.json() 



# escape special characters in user generated query params
# https://fragmentsofcode.wordpress.com/2010/03/10/escape-special-characters-for-solrlucene-query/
def solr_escape(value):
    if "SOLR_ESCAPE_REGEX" in current_app.config:
        ESCAPE_CHARS_RE = re.compile(current_app.config['SOLR_ESCAPE_REGEX'])
    else:
        ESCAPE_CHARS_RE = re.compile(r'(?<!\\)(?P<char>[&|+\-!(){}[\]^"~*?:])')
    return ESCAPE_CHARS_RE.sub(r'\\\g<char>', value)




#https://lucene.apache.org/solr/guide/8_6/json-request-api.html
def query(data):
    solr = get_solr() 
    solr = "/".join([solr,'query']) + '?q.op=AND'
    r = requests.post(solr,json=data)
    return r.json()      



# index a single document based on dict
def index_doc(data):
    solr = get_solr()
    solr = "/".join([solr,"update"])
    command = {"add":{"doc":data}}
    r = requests.post(solr,json=command )
    print (r.status_code)
    if r.status_code != 200:
        print (data)
    return r



def commit():
    solr = get_solr()
    solr_update = "/".join([solr,"update?commit=true"])
    r = requests.get(solr_update)
    print (r)



# delete all documents
def delete_all():
    solr = get_solr()
    solr_update = "/".join([solr,"update?commit=true"])
    r = requests.get(solr_update,json = {"delete": {"query": "*:*"}})



# helper function for editing solrconfig via solr config api
def config_solr(config_filename):
    solr_config = get_solr_config()
    with current_app.open_resource(config_filename) as j:
        data = json.load(j)
        for command in data:
            print (command)
            r = requests.post(solr_config,json=command)
            print (r)



# get solr query url
def get_solr ():
    if 'solr' not in g:
        g.solr = "/".join([current_app.config['SOLR'],current_app.config['SOLRCORE']])
    return g.solr


# get solr schema editing url 
def get_solr_schema ():
    if 'solr_schema' not in g:
        g.solr_schema = "/".join([current_app.config['SOLR_API'],'cores',current_app.config['SOLRCORE'],'schema'])
    return g.solr_schema



# get solr config api url
def get_solr_config ():
    if 'solr_config' not in g:
        g.solr_config = "/".join([current_app.config['SOLR_API'],'cores',current_app.config['SOLRCORE'],'config'])
    return g.solr_config



